(ns ri.core
;  (:refer-clojure :exclude [* - + == /])
  (:use [opennlp nlp])
;  (:use clojure.core.matrix)
;  (:require [clojure.core.matrix.operators :as ops])
  )

(def get-sentences (make-sentence-detector "models/en-sent.bin"))
(def tokenize (make-tokenizer "models/en-token.bin"))
(def detokenize (make-detokenizer "models/english-detokenizer.xml"))



(defn make-ri[dimensionality randomness lcw rcw]
  {:dimensionality dimensionality :randomness randomness :lcw lcw :rcw rcw :content {} :empty-vec (vec (repeat dimensionality 0))}
  )

(defn pick-n-random[col n]
  (loop [col col new-col []]
    (if (>= (count new-col) n)
      new-col
      (let [rand-index (rand-nth col)
            but-index (filter #(not= % rand-index) col)]
        (recur but-index (conj new-col rand-index))))))

(defn index-vector-elements[ri]
  (let [dimensionality (:dimensionality ri)
        randomness (:randomness ri)
        ind (pick-n-random (range dimensionality) (* 2 randomness))]
    {:neg-el (vec (take randomness ind)) :pos-el (vec (take-last randomness ind))}
    )
  )

(defn make-index-vector[ri label]
  (let [dimensionality (:dimensionality ri)
        randomness (:randomness ri)
        posels (:pos-el label)
        negels (:neg-el label)
        iv (vec (repeat dimensionality 0))
        iv (reduce #(assoc %1 %2 -1) iv negels)
        iv (reduce #(assoc %1 %2 1) iv posels)]
  iv
    )
  )


(defn make-label[ri]
  (let [label {}
        label (merge (index-vector-elements ri))
        iv (make-index-vector ri label)
        ]
    (assoc label :cv (:empty-vec ri) :iv iv)
    )
  )

(defn set-label[ri word label]
  (assoc-in ri [:content word] label))

(defn has-label? [ri word]
  (contains? (set (keys (:content ri))) word))


(defn generate-labels[ri words]
  (reduce (fn [ri word]
            (let [label ((:content ri) word)]
              (if (nil? label)
                (set-label ri word (make-label ri))
                ri
                )
              )
            ) ri words))

;(defn update-label [ri word label]
;  (let [orig-label ((:content ri) word)]
;    (assoc-in ri [:content word :cv]  (mapv + (:cv label) (:cv orig-label)))
;    )
;  )

;; time with matrix: 152.167, 156.205 seconds.
;; time with reduce: 9.635, 8.669 seconds (3.869 with agents, part-size 400)
(defn update-window[focus-label window-label weight]
  (let [cv (:cv focus-label)
        iv (:iv window-label)
;        iv (ops/* weight iv)

        with-pos (reduce (fn [cv ind]
                           (assoc cv ind (+ weight (nth cv ind))))
                   cv (:pos-el window-label))
        with-pos-and-neg (reduce (fn [cv ind]
                                   (assoc cv ind (- (nth cv ind) weight)))
                           with-pos (:neg-el window-label))
;
        ]
;    (assoc focus-label :cv (ops/+ iv cv))
    (assoc focus-label :cv with-pos-and-neg)
    )
  )


(defn update-with-left-labels[ri words]
  (reduce (fn [ri token-index]
            (let [word (nth words token-index)
                  label ((:content ri) word)
                  label
                  ;; update with left labels. we need to have made sure that those labels have been added to the
                  ;; space before hand
                  (reduce (fn [label l-window-index]
                            (let [lti (- token-index l-window-index 1)]
                              (if (< lti 0)
                                label
                                (let [lword (nth words lti)
                                      llab ((:content ri) lword)
                                      weight (nth (:lcw ri) l-window-index)]
                                  (update-window label llab weight)
                                  )
                                )
                              )
                            ) label (range (count (:lcw ri))))]
              (set-label ri word label)
              )
            ) ri (range (count words))))

(defn update-with-right-labels[ri words]
  (reduce (fn [ri token-index]
            (let [word (nth words token-index)
                  label ((:content ri) word)
                  label

                  ;; update with right labels. we need to have made sure that those labels have been added to the
                  ;; space before hand
                  (reduce (fn [label r-window-index]
                            (let [rti (+ token-index r-window-index 1)]
                              (if (>= rti (count words))
                                label
                                (let [rword (nth words rti)
                                      rlab ((:content ri) rword)
                                      weight (nth (:rcw ri) r-window-index)]
                                  (update-window label rlab weight)
                                  )
                                )
                              )
                            ) label (range (count (:rcw ri))))]

              (set-label ri word label)

              )
            ) ri (range (count words))))


(def agents (atom 0))

(defn add-text-thread[ri sentences]
  (let [ri (reduce (fn [ri sentence]
                     (let [tokens (tokenize sentence)]
                       (-> ri
                         (generate-labels tokens)
                         (update-with-left-labels tokens)
                         (update-with-right-labels tokens))
                       )
                     ) ri sentences)]
    (swap! agents inc)
  ri
    )

  )



(defn add-text [ri text]
  (reset! agents 0)
  (let [num-conc 400
        sentences (get-sentences text)
        sent-num (count sentences)
        sent-part (partition num-conc sentences)
;        ag1 (agent ri)
        ]

    (println "Adding" (count sentences) "sentences...")
    ; (doseq [p sent-part]
      ; (let [ag1 (agent ri)]
        ; (send-off ag1 add-text-thread (vec p))
        ; )
      ; )

    ; (loop []
      ; (if (>= @agents (count sent-part))
        ; (do 
		; (println @agents "agents completed their tasks.")
		; )
        ; (recur)
        ; )
      ; )
	  (reduce add-text-thread ri sent-part)
    )

  )

(defn get-cv [ri word]
  (:cv ((:content ri) word)))

;(def ri (agent (make-ri 100 2 [0.5 1] [0.5 1])))

(defn print-ri[ri]

  (println (str "Dimensionality: " (:dimensionality ri) "\nRandom Indices: " (:randomness ri) "\nContent:\n\n"))
  (doseq [kv (:content ri)]
    (let [iv (vec (repeat (:dimensionality ri) 0))
          iv (reduce #(assoc %1 %2 (inc (nth %1 %2))) iv (:pos-el (val kv)))
          iv (reduce #(assoc %1 %2 (dec (nth %1 %2))) iv (:neg-el (val kv)))]
    (println (str (key kv) "\n cv: " (:cv (val kv)) "\n iv: " iv )))))

(defn create-space [dimensionality in-f out-f]
  (let [pre-time (System/currentTimeMillis)
        ri (make-ri dimensionality 2 [0.5 1] [0.5 1])
;        a (println "Reading text...")
        in-text (slurp in-f)
;        a (println "Adding text...")
        ri (add-text ri in-text)
;        ri (reduce #(assoc-in %1 [:content (key %2)] (dissoc (val %2) :iv)) ri  (:content ri))
        ]
    (println "building space took" (/ (- (System/currentTimeMillis) pre-time) 1000.0) "seconds.")
;    (println "Saving space...")
;    (spit out-f ri)
    )
  )

;; 17.486s, 14.603s
(defn load-space [in-f]
  (println "Loading space...")
  (let [space (read-string (slurp in-f))]
    (reduce #(assoc-in %1 [:content (key %2) :iv] (make-index-vector %1 (val %2))) space  (:content space))
    )
  )
(defn test-func[])

;(def ri (read-string (slurp "out.space")))
;
;(get-cv ri "lands")

;(ops/*
;  (transpose
;    [
;    [0  1  1 0]
;    [1  0  1 -1]
;    [-1 -1 1 1]
;    [1  -1 1 1]
;    [0  1  1 0]
;
;    ]) [0.5 1 0 1 0.5])